<?php
class Pendaftaran extends CI_Controller {
	function __construct(){
		parent:: __construct();
		$this->load->model('Sop_Model');
		//$this->load->library('Bcrypt');
		$this->load->library('pdf');
		setlocale (LC_TIME, 'id_ID');
	}

	function index(){
		// $whr = '';
		// if ($)
		$data['open']='Pendaftaran/simpan_user';
		$data['kembali']='';
		$data['poli']=$this->Sop_Model->qw("*","master_poli","ORDER BY id ASC")->result();
		$data['dokter']=$this->Sop_Model->qw("*","master_dokter","ORDER BY id ASC")->result();
		$data['jadwal']=$this->Sop_Model->qw("j.id as idjadwal, j.*,d.nama,d.gelar,p.nama_poli","master_jadwal j left outer join master_dokter d on j.kode_dokter=d.kode_dokter 
		left outer join master_poli p on j.kode_poli=p.kode_poli where j.kode_poli='P004' or j.kode_poli='P007'","ORDER BY j.id ASC")->result();
		$this->load->view('index3',$data);
	}

	function page(){
		$page=$this->uri->segment(3);
		$data['page']=$page;
		if($page=="daftar_user"){
			$data['open']='Pendaftaran/simpan_user';
			$data['kembali']='';
			//$data['tmp_pt']=$this->Sop_Model->qw("*","user","")->result();
		}elseif($page=="lihat_form"){
			$data['tmp_pt']=$this->Sop_Model->qw("*","master_form","ORDER BY id ASC")->result();
		}
		$this->load->view('index3',$data);
	}

	function curl($url, $datareq){
		$ch = curl_init(); 
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $datareq);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		  // Set here requred headers
			"accept: */*",
			"accept-language: en-US,en;q=0.8",
			"content-type: application/json",
		));
		$output = curl_exec($ch); 
		curl_close($ch);      
		return $output;
		// dd($output);
	}

	public function curPostRequest()
    {
        /* Endpoint */
        $url = 'http://app.inhealth.co.id/pelkesws2/api/EligibilitasPeserta';
   
        /* eCurl */
        $curl = curl_init($url);
		$sekarang = date("Y-m-d");
        /* Data */
        $data = [
			"token"=>"inh_22009d5f2a1001a9812227f58c5faf9d",
			"kodeprovider"=>"1101G042",
			"nokainhealth"=>"1001460689482",
			"tglpelayanan"=>"2017-11-03",
			"jenispelayanan"=>"3",
			"poli"=>"GIG"
        ];
		print_r($data);
        $ch = curl_init(); 
		//curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($curl, CURLOPT_HTTPHEADER, array(
		  // Set here requred headers
			"accept: */*",
			"accept-language: en-US,en;q=0.8",
			"content-type: application/json",
		));
		$output = curl_exec($curl); 
		curl_close($curl);      
		echo $output;
    }

	function getasuransi($no_peserta){
		$sekarang = date("Y-m-d");
      // Data Parameter yang Dikirim oleh cURL
      $datareq = array(
        "token"=>"inh_22009d5f2a1001a9812227f58c5faf9d",
        "kodeprovider"=>"1101G042",
        "nokainhealth"=>$no_peserta,
        "tglpelayanan"=>$sekarang,
        "jenispelayanan"=>"3",
        "poli"=>"GIG"
      );
      $send = $this->curl("http://app.inhealth.co.id/pelkesws2/api/EligibilitasPeserta",json_encode($datareq));
      // dd(json_encode($data));
      $konten = $send;
    echo $konten;
	//  print_r($konten);
	}
	function getmhsw(){
		$tgl_lahir = $this->input->post('datepickerc');
		$nama = $this->input->post('namamhsw');
		// $data = "https://sima.usm.ac.id/rest_api/rest_ltc/mhs/".$nim;
  		// $hasil = file_get_contents($data);
  		// $hasil = trim(preg_replace('/\s\s+/', '', $hasil));
  		// $hasil=str_replace(array("\r\n","\r","\n"),"",$hasil);
  		// $hasil= str_replace("Array(","{",$hasil);
  		// $hasil= str_replace("[",',"',$hasil);
  		// $hasil= str_replace("] => ",'" : "',$hasil);
  		// $hasil= str_replace(',"','","',$hasil);
  		// $hasil= str_replace(')','"}',$hasil);
  		// $hasil= str_replace('{",','{',$hasil);
  		// $hasil= str_replace(': "{',':{',$hasil);
  		// $hasil= str_replace('}"','}',$hasil);
		  $hasil = $this->Sop_Model->getInfoPasien($nama,$tgl_lahir);
		//   $hasil = $this->Sop_Model->getInfoPasien("Bagas Lutfi Alfat","1994-08-09");
		  echo json_encode($hasil);	
	}
	function getjadwal(){
		//$nim = $this->input->post('rekam_medis');
		$poli = $this->input->post('poli');
		$dokter = $this->input->post('dokter');
		$tgl_periksa = $this->input->post('tgl_periksa');
		$hasil = $this->Sop_Model->getInfoJadwal($poli,$dokter,$tgl_periksa);
		echo json_encode($hasil);
	}

	function lokasi_file($jenis_file,$id_ijin)
	{
		$getfile=$this->Sop_Model->qw("*","file_peserta","WHERE id_ijin='$id_ijin' AND jenis_file='$jenis_file'")->row_array();
		return $getfile;
	}

	function simpanpdf(){
		$no_antrian= $this->input->post('no_antrian');
		$nama_poli= $this->input->post('nama_poli');
		$nama_dokter= $this->input->post('nama_dokter');
		$hari_dantgl= $this->input->post('hari_dantgl');
		$tanggal_lahir= $this->input->post('tanggal_lahirpdf');
		$nama_pasien= $this->input->post('nama_pasien');
		$rekam_medis= $this->input->post('rekam_medispdf');
		$jam_periksa= $this->input->post('jam_periksa');
		$pdf = new FPDF('l','mm','A5');
        // membuat halaman baru
        $pdf->AddPage();
        // setting jenis font yang akan digunakan
        $pdf->SetFont('Arial','B',16);
        // mencetak string
		$pdf->Image(base_url().'assets/image/logoklinik.png',10,5,25,25);
        $pdf->Cell(190,7,'3K Dental Care',0,1,'C');
        $pdf->SetFont('Arial','B',12);
        $pdf->Cell(190,7,'Kartu Pendaftaran Reservasi Online ',0,1,'C');
        // Memberikan space kebawah agar tidak terlalu rapat
        $pdf->Cell(10,7,'',0,1);
        $pdf->SetFont('Arial','',10);
        $pdf->Cell(50,6,'No. Pendaftaran',1,0);
        $pdf->Cell(10,6,':',1,0);
		$pdf->SetFont('Arial','B',10);
        $pdf->Cell(85,6,$no_antrian,1,0);
		$pdf->Ln();
        $pdf->SetFont('Arial','',10);
		$pdf->Cell(50,6,'Nama Poli',1,0);
        $pdf->Cell(10,6,':',1,0);
		$pdf->SetFont('Arial','B',10);
        $pdf->Cell(85,6,$nama_poli,1,0);
		$pdf->Ln();
        $pdf->SetFont('Arial','',10);
		$pdf->Cell(50,6,'Nama Dokter',1,0);
        $pdf->Cell(10,6,':',1,0);
		$pdf->SetFont('Arial','B',10);
        $pdf->Cell(85,6,$nama_dokter,1,0);
		$pdf->Ln();
        $pdf->SetFont('Arial','',10);
		$pdf->Cell(50,6,'Hari / Tgl. Periksa',1,0);
        $pdf->Cell(10,6,':',1,0);
		$pdf->SetFont('Arial','B',10);
        $pdf->Cell(85,6,$hari_dantgl,1,0);
		$pdf->Ln();
        $pdf->SetFont('Arial','',10);
		$pdf->Cell(50,6,'Jam Periksa',1,0);
        $pdf->Cell(10,6,':',1,0);
		$pdf->SetFont('Arial','B',10);
        $pdf->Cell(85,6,$jam_periksa,1,0);
		$pdf->Ln();
        $pdf->SetFont('Arial','',10);
		$pdf->Cell(50,6,'Rekam Medis',1,0);
        $pdf->Cell(10,6,':',1,0);
		$pdf->SetFont('Arial','B',10);
        $pdf->Cell(85,6,$rekam_medis,1,0);
		$pdf->Ln();
        $pdf->SetFont('Arial','',10);
		$pdf->Cell(50,6,'Nama Pasien',1,0);
        $pdf->Cell(10,6,':',1,0);
		$pdf->SetFont('Arial','B',10);
        $pdf->Cell(85,6,$nama_pasien,1,0);
		$pdf->Ln();
        $pdf->SetFont('Arial','',10);
		$pdf->Cell(50,6,'Tgl. Lahir Pasien',1,0);
        $pdf->Cell(10,6,':',1,0);
		$pdf->SetFont('Arial','B',10);
        $pdf->Cell(85,6,$tanggal_lahir,1,0);
		$pdf->Ln();
		$pdf->Ln();
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(50,6,'NB : Nomor Pendaftaran diatas belum terkonfirmasi sebelum dilakukan konfirmasi ditempat',0,1);

        $pdf->Output('D',$nama_pasien.'-'.$no_antrian.'.pdf',true);
	}
	function simpanjadwal(){
		$tipe_pasien= $this->input->post('tipe_pasien');
		$rekam_medis= $this->input->post('rekam_medis');
		$nama= $this->input->post('nama');
		$nopeserta= $this->input->post('nopeserta1');
		$asuransi= $this->input->post('asuransi1');
		$jenis_pasien='';
		if($asuransi=='inhealth' || !empty($nopeserta)) $jenis_pasien='2';
		if($asuransi=='umum' || empty($nopeserta)) $jenis_pasien='1';
		$tempat_lahir= $this->input->post('tempat_lahir');
		$tanggal_lahir= $this->input->post('tanggal_lahir');
		$tgl_periksa= $this->input->post('tgl_periksa');
		$hp= $this->input->post('hp');
		$alamat= $this->input->post('alamat');
		$kode_jadwal= $this->input->post('idfinal');
		$status=false;
		$id_datapasien = '';
		if ($tipe_pasien == 'lama') {
			if(empty($rekam_medis) || $rekam_medis=='' || !isset($rekam_medis)) {
				$err = 1;
        		$pesan = "No. Rekam Medis tidak sesuai";
				$status=false;
			} else {
				//cek no antri
				$key['kode_jadwal'] = $kode_jadwal;
				$key['tanggal'] = $tgl_periksa;
				$cekantrian = $this->Sop_Model->getData('jadwal_harian',$key);
				if ($cekantrian->num_rows() > 0) {
					foreach ($cekantrian->result() as $row)
					{
        				$id_jadwal = $row->id;
					}
				} else {
					$ary=array(
						'kode_jadwal'	=>$kode_jadwal,
						'tanggal'	=>$tgl_periksa,
						'validasi' =>'0'
					);
					$this->Sop_Model->simpan_tambah_kelas('jadwal_harian',$ary);
					$id_jadwal = $this->db->insert_id();
				}
				$whr['id_jadwal'] = $id_jadwal;
				$whr['rekam_medis'] = $rekam_medis;
				$cekDatapsien = $this->Sop_Model->getData('data_pasien',$whr);
				if ($cekDatapsien->num_rows() > 0) {
					// $err = 2;
					// $pesan = "Data Sudah Ada";
					// $status=false;
					foreach ($cekDatapsien->result() as $row)
					{
						$antrian = $row->no_antri;
					}
					$pesan = "Data Sudah Ada";
					$err = 0;
					$status=true;
				} else {
					$whr2['id_jadwal'] = $id_jadwal;
					$antrian = $this->Sop_Model->getAntrian('data_pasien',$whr2);
					$ary2=array(
						'id_jadwal'	=>$id_jadwal,
						'rekam_medis'	=>$rekam_medis,
						'no_antri' =>$antrian,
						'status'		=>'1',
						'pasien_baru'=>'0'
					);
					$this->Sop_Model->simpan_tambah_kelas('data_pasien',$ary2);
					//$id_datapasien = $this->db->insert_id();
					$pesan = "Data Berhasil Di Simpan";
					$err = 0;
					$status=true;
				}	
			}
		} else {
			//cek calon rekam medis
			// $cekCalon = $this->db->query('select max(');
			// if ($cekCalon)
			$ary=array(
				'nama'	=>$nama,
				'tempat_lahir'	=>$tempat_lahir,
				'tanggal_lahir' =>$tanggal_lahir,
				'alamat' =>$alamat,
				'jenis_pasien' =>$jenis_pasien,
				'no_asuransi' =>$nopeserta,
				'hp' =>$hp,
				'status' =>'0'
			);
			$this->Sop_Model->simpan_tambah_kelas('calon_pendaftar',$ary);
			$id_pendaftar = $this->db->insert_id();
			$key['kode_jadwal'] = $kode_jadwal;
			$key['tanggal'] = $tgl_periksa;
				$cekantrian = $this->Sop_Model->getData('jadwal_harian',$key);
				if ($cekantrian->num_rows() > 0) {
					foreach ($cekantrian->result() as $row)
					{
        				$id_jadwal = $row->id;
					}
				} else {
					$ary=array(
						'kode_jadwal'	=>$kode_jadwal,
						'tanggal'	=>$tgl_periksa,
						'validasi' =>'0'
					);
					$this->Sop_Model->simpan_tambah_kelas('jadwal_harian',$ary);
					$id_jadwal = $this->db->insert_id();
				}
				$whr['id_jadwal'] = $id_jadwal;
				$antrian = $this->Sop_Model->getAntrian('data_pasien',$whr);
				$whr['rekam_medis'] = $id_pendaftar;
				$cekDatapsien = $this->Sop_Model->getData('data_pasien',$whr);
				if ($cekDatapsien->num_rows>0){
					$err = 0;
					$pesan = "Data Sudah Ada";
					$status=true;
				} else {
					$ary2=array(
						'id_jadwal'	=>$id_jadwal,
						'id_calon'	=>$id_pendaftar,
						'no_antri' =>$antrian,
						'status'		=>'1',
						'pasien_baru'=>'1'
					);
					$this->Sop_Model->simpan_tambah_kelas('data_pasien',$ary2);
					//$id_datapasien = $this->db->insert_id();
					$pesan = "Data Berhasil Di Simpan";
					$err = 0;
					$status=true;
				}
		}
		if (empty($rekam_medis) || $rekam_medis=='') {
			$nama_pasien = $nama;
		} else {
			$getNama=$this->Sop_Model->qw("nama,tanggal_lahir","rekam_medis where rekam_medis='$rekam_medis'","")->row();
			$nama_pasien = $getNama->nama;
			$tanggal_lahir = $getNama->tanggal_lahir;
		}
		// $whr2['id'] = $id_datapasien;
		// $antrian = $this->Sop_Model->getData('data_pasien',$whr2);
		$jadwal=$this->Sop_Model->qw("j.id as idjadwal, j.*,d.nama,d.gelar,p.nama_poli","master_jadwal j left outer join master_dokter d on j.kode_dokter=d.kode_dokter 
		left outer join master_poli p on j.kode_poli=p.kode_poli where j.id='$kode_jadwal'","ORDER BY j.id ASC")->result();
		foreach($jadwal as $tampil){
			$nama_poli = $tampil->nama_poli;
			$nama_dokter = $tampil->nama;
			$hari_angka = $tampil->hari;	
			$jam_periksa = $tampil->jam_mulai.' - '.$tampil->jam_selesai;
		}
		$hari = $this->getHari($hari_angka);
		$data = array(
			'no_antrian' =>$antrian,
			'nama_poli' => $nama_poli,
			'nama_dokter' => $nama_dokter,
			'rekam_medis' => $rekam_medis,
			'tgl_periksa' => $tgl_periksa,
			'jam_periksa' => $jam_periksa,
			'nama_pasien' => $nama_pasien,
			'tanggal_lahir' => $tanggal_lahir,
			'hari' => $hari
		);
		echo json_encode(
            // using the array for returning data
            array(
                'status' => $status,
				'pesan'=> $pesan,
				'err' => $err,
                'data' => $data
            )
        );
	}
	function getHari($hari){
		switch ($hari) {
			case 0:
				$h="Minggu";
				break;
			case 1:
				$h="Senin";
				break;
			case 2:
				$h="Selasa";
				break;
			case 3:
				$h="Rabu";
				break;
			case 4:
				$h="Kamis";
				break;
			case 5:
				$h="Jumat";
				break;
			case 6:
				$h="Sabtu";
				break;
		}
		  return $h;
	}
	function simpan_user(){
		$nama = $this->input->post('nama');
		$namamhsw = $this->input->post('namamhsw');
		
		$alamat = $this->input->post('alamat');
		$nim = $this->input->post('nim');
		$kodekhusus = $this->input->post('kodekhusus');
		$prodi = $this->input->post('prodi');
		$fak = $this->input->post('fak');
		$telp = $this->input->post('telp');
		$email = $this->input->post('email');
		$password = $this->input->post('password');
		$password_en = password_hash($password, PASSWORD_DEFAULT);
		$tahun = date("Y") . "" . 1;
		if(empty($nama) || $nama=='' || !isset($nama)) $nm=$namamhsw; else $nm=$nama;
		//echo "namanya ".$nm;
		//exit();
		$hitung=$this->Sop_Model->qw("*","tblsiswa","WHERE nis='$email'"
        )->num_rows();

        if($hitung>0)
        {
        	$error = 1;
        	redirect('Pendaftaran/page/daftar_user/'.$error);
        }

        if(strlen($password)<8)
        {
        	$error = 2;
        	redirect('Pendaftaran/page/daftar_user/'.$error);
        }
		
		//memasukkan table kelas
		$ary=array(
			'Kelas'	=>$email,
			'nip_wali'	=>'',
			'tahun' =>$tahun,
			'Active'		=>'Y'
		);
		$this->Sop_Model->simpan_tambah_kelas('tblkelas',$ary);
		$insert_id = $this->db->insert_id();

		$ary2=array(
			'nis'		=>$email,
			'nama'		=>$nm,
			'IDKelas' 	=>$insert_id,
			'email'		=>$email,
			'alamat'	=>$alamat,
			'status'	=>'Y',
			'agama'		=>'',
			'nim'		=>$nim,
			'kodekhusus'		=>$kodekhusus,
			'prodi'		=>$prodi,
			'fak'		=>$fak
		);
		$this->Sop_Model->simpan_tambah_siswa('tblsiswa',$ary2);
		$insert_id2 = $this->db->insert_id();

		$ary3=array(
			'id'			=>$insert_id2,
			'username'		=>$email,
			'password' 		=>$password_en,
			'status'		=>1,
			'jurusan'		=>1,
			'nis'			=>$email,
			'nama'			=>$nm,
			'aktif'			=>0
		);
		$this->Sop_Model->simpan_tambah_peserta('t_peserta',$ary3);
		redirect('Pendaftaran/page/daftar_user/3');
	}

    function logout(){
		$this->session->sess_destroy();
		redirect('LoginAdmin_Controller');
	}
}
