<?php
	class Sop_Model extends CI_Model{
		function qw($cel,$table,$prop){
			return $this->db->query("SELECT $cel FROM $table $prop");
		}
		public function tgl_str($date){
			$exp = explode('-',$date);
			if(count($exp) == 3) {
				$date = $exp[2].'-'.$exp[1].'-'.$exp[0];
			}
			return $date;
		}
		public function getInfoPasien($nama,$tgl_lahir)
  {
    // $key['rekam_medis'] = $id;
	$get = $this->db->query("select rekam_medis.*, data_inhealth.no_peserta, data_inhealth.kode_produk, data_inhealth.kode_kelas from rekam_medis 
	left outer join data_inhealth on rekam_medis.rekam_medis=data_inhealth.rekam_medis 
	where rekam_medis.nama like '%$nama%' and rekam_medis.tanggal_lahir='$tgl_lahir'");
    // $get = $this->db->get_where('rekam_medis',$key);
    if($get->num_rows()>0)
    {
      $row = $get->row();
      $hasil = array(
        'id' => $row->id,
        'rekam_medis' => $row->rekam_medis,
        'no_identitas' => $row->no_identitas,
        'nama' => $row->nama,
        'jenis_kelamin' => $row->jenis_kelamin,
        'tempat_lahir' => $row->tempat_lahir,
        'tanggal_lahir' => $this->tgl_str($row->tanggal_lahir),
        'alamat' => $row->alamat,
        'hp' => $row->hp,
		'jenis_pasien' => $row->jenis_pasien,
		'no_peserta' => $row->no_peserta
      );
    }else{
      $hasil = array(
        'id' => '',
        'rekam_medis' => '',
        'no_identitas' => '',
        'nama' => '',
        'jenis_kelamin' => '',
        'tempat_lahir' => '',
        'tanggal_lahir' => '',
        'alamat' => '',
        'hp' => '',
		'jenis_pasien' => '',
		'no_peserta' => ''
      );
    }
		return $hasil;
	}
	public function getAntrian($table,$where) {
		$this->db->select_max('no_antri');
		$this->db->where($where);
		$this->db->from($table);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row)
			{
				$no_antrian = $row->no_antri + 1;
				// $no_antrian = $row->no_antri;
			}
		} else $no_antrian = 1;
		return $no_antrian;
	}
	public function getAll($table) {
		$get = $this->db->get_where($table);
		return $get;
	}
	public function getData($table,$where) {
		$get = $this->db->get_where($table,$where);
		return $get;
	}
	public function getInfoJadwal($poli,$dokter,$tgl_periksa)
  	{
		$whr = '';
		if (!empty($poli)) $whr .= "where j.kode_poli = '$poli' "; else $whr .= "where (j.kode_poli='P004' or j.kode_poli='P007') ";
		if (!empty($dokter)) $whr .= "and j.kode_dokter = '$dokter' ";
		if (!empty($tgl_periksa)) { 
			$timestamp = strtotime($tgl_periksa);
			$day = date('w', $timestamp);
			$whr .= " and j.hari = '$day'";
		}
    	$get = $this->db->query("select j.id as idjadwal, j.*,d.nama, d.gelar, p.nama_poli from master_jadwal j 
		left outer join master_dokter d on j.kode_dokter=d.kode_dokter 
		left outer join master_poli p on j.kode_poli=p.kode_poli 
		left outer join master_cuti c on d.kode_dokter=c.kode_dokter $whr ");
    	if($get->num_rows()>0)
    	{
      		// $row = $get->row();
			// if()
      		// $hasil = array(
        	// 	'id' => $row->id,
        	// 	'kode_dokter' => $row->kode_dokter,
        	// 	'nama' => $row->nama,
        	// 	'kode_poli' => $row->kode_poli,
        	// 	'nama_poli' => $row->nama_poli,
        	// 	'hari' => $hari,
        	// 	'hari_angka' => $row->hari,
        	// 	'jam_mulai' => $row->jam_mulai,
        	// 	'jam_selesai' => $row->jam_selesai
      		// );
			//  return $get->result_array();
			$arr = array();
            foreach ($get->result() as $row) {
			$hari = $this->convertHari($row->hari);
			$cekPendaftar = $this->db->query("select * from data_pasien 
			where id_jadwal IN (select id from jadwal_harian 
			where kode_jadwal='".$row->idjadwal."' and tanggal='".$tgl_periksa."')");
			$pendaftar = $cekPendaftar->num_rows();
			$arr[] = array(
				'idjadwal' => $row->idjadwal,
        		'kode_dokter' => $row->kode_dokter,
        		'nama' => $row->nama.', '.$row->gelar,
        		'kode_poli' => $row->kode_poli,
        		'nama_poli' => $row->nama_poli,
        		'hari' => $hari,
        		'hari_angka' => $row->hari,
        		'jam_mulai' => $row->jam_mulai,
        		'jam_selesai' => $row->jam_selesai,
				'quota' => $pendaftar.'/'.$row->quota
				 );
			}
			return $arr;
    	}else{
			return array();
    	}
  	}
	function convertHari($hari){
                  switch ($hari) {
                    case 0:
                        $h= "Minggu";
                        break;
                    case 1:
                        $h="Senin";
                        break;
                    case 2:
                        $h="Selasa";
                        break;
                    case 3:
                        $h="Rabu";
                        break;
                    case 4:
                        $h="Kamis";
                        break;
                    case 5:
                        $h="Jumat";
                        break;
                    case 6:
                        $h="Sabtu";
                        break;
                }
				return $h;
	}
		function simpan_user($table,$value){
			return $this->db->insert($table,$value);
		}
		function simpan_tambah_kelas($table,$value){
			return $this->db->insert($table,$value);
		}
		function simpan_tambah_siswa($table,$value){
			return $this->db->insert($table,$value);
		}
		function simpan_tambah_peserta($table,$value){
			return $this->db->insert($table,$value);
		}
		function simpan_materi($table,$value){
			return $this->db->insert($table,$value);
		}
		function simpan_tambah_pertemuan($table,$value){
			return $this->db->insert($table,$value);
		}
		function simpan_peserta_ujian($table,$value){
			return $this->db->insert($table,$value);
		}
		function simpan_dimensi($table,$value){
			return $this->db->insert($table,$value);
		}
		function simpan_komponen_nilai($table,$value){
			return $this->db->insert($table,$value);
		}
		function simpan_tambah_kegiatan($table,$value){
			return $this->db->insert($table,$value);
		}
		function simpan_master_jawaban($table,$value){
			return $this->db->insert($table,$value);
		}
		function simpan_tambah_paket($table,$value){
			return $this->db->insert($table,$value);
		}
		function simpan_tambah_pisma2($table,$value){
			return $this->db->insert($table,$value);
		}
		function simpan_presensi_pisma2($table,$value){
			return $this->db->insert($table,$value);
		}
		function edit_odha($table,$where,$value){
			$this->db->where('id',$where);
			return $this->db->update($table,$value);
		}
		function ubah_notifikasi($table,$where,$value){
			$this->db->where('id',$where);
			return $this->db->update($table,$value);
		}
		function reset_password($table,$where,$value){
			$this->db->where('username',$where);
			return $this->db->update($table,$value);
		}
		function edit_status($table,$where,$value){
			$this->db->where('id',$where);
			return $this->db->update($table,$value);
		}
		function simpan_upload_bukti($table,$where,$value){
			$this->db->where('id_peserta_ujian',$where);
			return $this->db->update($table,$value);
		}
		function edit_ujian($table,$where,$value){
			$this->db->where('id',$where);
			return $this->db->update($table,$value);
		}
		function edit_materi($table,$where,$value){
			$this->db->where('id',$where);
			return $this->db->update($table,$value);
		}
		function ubah_status_plot($table,$where,$value){
			$this->db->where('id',$where);
			return $this->db->update($table,$value);
		}
		function ubah_kegiatan($table,$where,$value){
			$this->db->where('id',$where);
			return $this->db->update($table,$value);
		}
		function data_pembimbing($table,$where){
			$this->db->where('data_pembimbing',$where);
			return $this->db->delete($table);
		}
		function hapus_kegiatan($table,$where){
			$this->db->where('id',$where);
			return $this->db->delete($table);
		}
		function hapus_peserta($table,$where){
			$this->db->where('id',$where);
			return $this->db->delete($table);
		}
		function hapus_art($table,$where){
			$this->db->where('id',$where);
			return $this->db->delete($table);
		}
		function hapus_materi($table,$where){
			$this->db->where('id',$where);
			return $this->db->delete($table);
		}
		public function convert_nilai($jawaban_benar,$id_bidang)
	{
		$q = $this->db
				->select('jawaban_benar, listening, structure, reading')
				->from('t_konversinilai')
				->where('jawaban_benar', $jawaban_benar)
				->get()
				->row()
				;
		
		if($id_bidang == 1 || $id_bidang == 2 || $id_bidang == 3 ) {
			$nilai = $q->listening;
		} elseif ($id_bidang == 4 || $id_bidang == 5 || $id_bidang == 6 ) {
			$nilai = $q->structure;
		} elseif ($id_bidang == 7 || $id_bidang == 8 || $id_bidang == 9 ) {
			$nilai = $q->reading;
		}
		//echo 'jwaban = '.$jawaban_benar.' kode matkul = '.$id_bidang.' nilai='.$nilai;
		//exit();
		return $nilai;
	}

	}
?>
