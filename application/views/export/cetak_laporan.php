<?php
// Fungsi header dengan mengirimkan raw data excel
header("Content-type: application/vnd-ms-excel");

// Mendefinisikan nama file ekspor "hasil-export.xls"
header("Content-Disposition: attachment; filename=laporan_permohonan.xls");
?>
<section class="content-header">

      <h1>
        Data Permohonan
      </h1>
</section>



   <section class="content">

      <div class="row">

        <div class="col-xs-12">

          <div class="box box-primary">

            <div class="box-body">

              <table id="example2" class="table table-bordered table-striped display nowrap" border="1">

                <thead>

                <tr style="background-color: #CCCCCC">

                  <th>No</th>

                  <th>NIP</th>

                  <th>Nama</th>
                  <th>Unit</th>
                  <th>Tanggal</th>
                  <th>Status Verifikasi</th>

                </tr>

                </thead>

                <tbody>

                <?php

                  $no=0;

                  foreach($tmp_pt as $tampil){

                  $no++;

                ?>

                <tr>

                  <td><?php echo $no;?></td>

                  <td><?php echo $tampil->nip;?></td>

                  <td><?php
                  $CI =& get_instance();
                            $status = $CI->get_pegawai($tampil->nip);
                            $nama_pegawai = $status['nama_pejabat'];
                            $unit = $status['unker'];
                            echo $nama_pegawai;
                  ?></td>
                  <td><?php echo $unit;?></td>
                  <td><?php
                  echo date("d-m-Y", strtotime($tampil->tanggal_input));
                  ?></td>

                  <td align="center">
                    <?php
                    if($tampil->status=='0')
                    {
                      ?>
                      <div style="color: red">Belum Diverifikasi</div>
                      <?php
                    }else{
                      ?>
                      <div style="color: green">Sudah Diverifikasi</div>
                      <?php
                    }
                    ?>
                  </td>

                </tr>

                <?php } ?>

                </tbody>

              </table>

            </div>

            <!-- /.box-body -->

          </div>

          <!-- /.box -->

        </div>

        <!-- /.col -->

      </div>

      <!-- /.row -->

    </section>