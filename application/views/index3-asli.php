<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>3K Dental Care</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/dist/css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Morris chart -->
  <!-- Date Picker -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/timepicker/bootstrap-timepicker.min.css">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/bootstrap-daterangepicker/daterangepicker.css">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <!-- bootstrap wysihtml5 - text editor -->
 

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/dist/css/font.css">
</head>
<body class="hold-transition skin-green sidebar-mini" >
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="<?php echo site_url('Pendaftaran');?>" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><img src="<?php echo base_url();?>assets/image/logo-header-small.png" alt="Logo Dental care"></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><img src="<?php echo base_url();?>assets/image/logo-header.png" alt="Logo Dental care"></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <li class="dropdown user user-menu">
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?php echo base_url();?>assets/dist/img/avatar5.png" class="img-circle" alt="User Image">

                <p>
                  <?php
                  //identifikasi levgel
                  
                  echo $nama_user;
                  if($leveling=='dosen')
                  {
                    echo ", ";
                    echo $gelar;
                  }
                  echo $this->session->userdata('nama_user');?><br><small><?php
                  echo $as;
                  echo "<br>";
                  echo $this->session->userdata('opd');
                  echo "<br>";
                  //echo "<br>";
                  //echo $this->session->userdata('rumpun_jabatan');
                  //echo "<br>";
                  //echo $this->session->userdata('jabatan');
                  ?>
                  <br><?php //echo date('d-M-Y');?></small>
                </p>
              </li>
              <!-- Menu Body -->
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-right">
                  <a href="<?php echo site_url('LoginAdmin_Controller/logout');?>" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
      </div>
      <!-- search form -->
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="">
          <a href="">
            <i class="fa fa-dashboard"></i> <span>Reservasi Online</span>
          </a>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <!-- <?php
      //include "content/".$page.".php";
    ?> -->


<style>
  #showNama {
  display: none; // you're using diaplay!
}
</style>
<?php
$error=$this->uri->segment(4);
//header('Content-Type: application/json');
header('Access-Control-Allow-Origin: *');
?>
<section class="content-header">
      <h1>
       Reservasi Online
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Form</a></li>
        <li class="active">Reservasi Online</li>
      </ol>
    </section>

    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">

            <?php
            if($error=='1')
            {
              ?>
              <br>
              <div class="callout callout-danger style="margin-left: 10px; margin-right: 10px">
              <h4>Email sudah pernah didaftarkan</h4>
              <p>Gunakan email yang lain untuk melakukan pendaftaran user</p>
              </div>
              <?php
            }elseif($error=='2')
            {
              ?>
              <br>
              <div class="callout callout-danger style="margin-left: 10px; margin-right: 10px">
              <h4>Pasword kurang dari 8 (delapan) karakter</h4>
              <p>Masukkan password minimal 8 (delapan) kombinasi karakter</p>
              </div>
              <?php
            }elseif($error=='3')
            {
              ?>
              <br>
              <div class="callout callout-success style="margin-left: 10px; margin-right: 10px">
              <h4>Pendaftaran User Berhasil!</h4>
              <p>Pendaftaran User telah berhasil, Silahkan login menggunakan username (Email) dan password yang telah didaftarkan untuk memilih jadwal ujian yang ingin diikuti</p>
              </div>
              <?php
            }
            ?>

            <form role="form" class="form-horizontal" action="#" method="POST" enctype="multipart/form-data">
              <div class="box-body">
                <div class="row">
                  <div class="col-md-6">
                   <!--<div class="form-group">
                      <label class="col-sm-3">Tipe Pendaftar</label>
                      <div class="col-sm-4">
                      <select onclick="status_change();" class="custom-select rounded-0" name="pendaftar" id="pendaftar">
                      <option value="mahasiswa">Mahasiswa USM</option>
                      <option value="umum" >Umum</option>
                      </div>
                    </div> -->
                    <div class="form-group">
                  <label  class="col-sm-3" for="exampleSelectBorder">Tipe Pasien</label>
                  <div class="col-sm-9">
                  <select class="custom-select form-control-border" onclick="status_change();" name="pendaftar" id="pendaftar">
                  <option value="lama">Lama</option>
                      <option value="baru" >Baru</option>
                  </select>
          </div>
                </div>
               
                <div id="kategdaftar" style="display: inline;">
                    <div class="form-group">
                      <label class="col-sm-3">Masukan Nama</label>
                      <div class="col-sm-9">
                        <input type="text" class="form-control" name="namamhsw" id="namamhsw" autocomplete="off">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-3">Masukkan Tanggal Lahir</label>
                      <div class="col-sm-9">
                      <input type="text" id="datepickerc" name="datepickerc" class="form-control">
                      </div>
                    </div>
                    <button type="button" id="btnCari" name="btnCari" class="btn btn-primary"><i class="fa fa-search"></i> Cari</button>
                    <hr>
                    <div class="form-group">
                      <label class="col-sm-3">No. Rekam Medis</label>
                      <div class="col-sm-9">
                      <span id="rekam_medis"></span>  
                      <input type="hidden" class="form-control" name="nim" id="nim" >
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-3">Tempat, Tanggal Lahir</label>
                      <div class="col-sm-9">
                      <span id="kode_khusus"></span>
                        <input type="hidden" class="form-control" name="kodekhusus" id="kodekhusus">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-3">Alamat</label>
                      <div class="col-sm-9">
                      <span id="prodi2"></span>
                        <input type="hidden" class="form-control" name="prodi" id="prodi">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-3">No.HP</label>
                      <div class="col-sm-9">
                      <span id="fak2"></span>
                        <input type="hidden" class="form-control" name="fak" id="fak">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-3">Jenis Pasien</label>
                      <div class="col-sm-9">
                      <span id="jns_pasien"></span>
                        <input type="hidden" class="form-control" name="jnspasien" id="jnspasien">
                      </div>
                    </div>
                    <!--Mulai cek jenis pasien, asuransi atau umum -->
                    <div id="showNomerpeserta">
                    <div class="form-group">
                      <label class="col-sm-3">No. Peserta</label>
                      <div class="col-sm-9">
                      <span id="no_peserta"></span>
                        <input type="hidden" class="form-control" name="nopeserta" id="nopeserta">
                      </div>
                    </div>
          </div>
          <!--Finish cek jenis pasien, asuransi atau umum -->
                    </div>
                    <div id="showNama">
                    <div class="form-group">
                      <label class="col-sm-3">Nama</label>
                      <div class="col-sm-9">
                        <input type="text" class="form-control" name="nama_lengkap" id="nama_lengkap" autocomplete="off">
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label class="col-sm-3">Tempat Lahir</label>
                      <div class="col-sm-9">
                      <input type="text" class="form-control" name="tempat_lahir" id="tempat_lahir" autocomplete="off">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-3">Tanggal Lahir</label>
                      <div class="col-sm-9">
                      <input type="text" id="datepickerb" name="datepickerb" class="form-control">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-3">Alamat</label>
                      <div class="col-sm-9">
                        <textarea class="form-control" name="alamat" id="alamat"></textarea>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-3">No. Telp / HP</label>
                      <div class="col-sm-4">
                        <input type="text" class="form-control" name="hp" id="hp" autocomplete="off">
                      </div>
                    </div>
                    <div class="form-group">
                  <label  class="col-sm-3" for="exampleSelectBorder">Jenis Pasien</label>
                  <div class="col-sm-9">
                  <select class="custom-select form-control-border" onclick="asuransi_change();" name="asuransi" id="asuransi">
                  <option value="umum">Umum</option>
                      <option value="inhealth" >Asuransi Inhealth Managed Care</option>
                      <option value="inhealthi" >Asuransi Inhealth Indemnity</option>
                  </select>
          </div>
                </div>
                <div id="kategasuransi">
                    <div class="form-group">
                      <label class="col-sm-3">No. Peserta Asuransi</label>
                      <div class="col-sm-9">
                        <input type="text" class="form-control" name="no_asuransi" id="no_asuransi" autocomplete="off" onChange="cekasuransi_change();">
                      </div>
                      
                    </div>
                    <div class="form-group">
                      <label class="col-sm-3">Status</label>
                      <div class="col-sm-9">
                      <span id="status_asuransi"></span>  
                      <input type="hidden" class="form-control" name="statusasuransi" id="statusasuransi" >
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-3">Atas Nama</label>
                      <div class="col-sm-9">
                      <span id="atas_nama"></span>  
                      <input type="hidden" class="form-control" name="atasnama" id="atasnama" >
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-3">Tanggal Lahir</label>
                      <div class="col-sm-9">
                      <span id="tgllahir_asuransi"></span>  
                      <input type="hidden" class="form-control" name="tgllahir_asuransi" id="tgllahir_asuransi" >
                      </div>
                    </div>
          </div>
                    </div>
                    
                    <div class="form-group">
                      <label class="col-sm-3"></label>
                      <div class="col-sm-9">
                        <b><br>Pilih Jadwal Periksa</b>
                      </div>
                    </div>
                    <div class="form-group">
                  <label  class="col-sm-3" for="exampleSelectBorder">Pilih Poli</label>
                  <div class="col-sm-9">
                  <select class="custom-select form-control-border"  name="poli" id="poli">
                  <option value="">Pilih Semua</option>
										<?php
										$no=0;
										foreach($poli as $tampil){
										$no++;
                    					echo '<option value="'. $tampil->kode_poli.'">'. $tampil->nama_poli.'</option>';
               							 } ?>
										</select>
          </div>
                </div>
                    
                    <div class="form-group">
                  <label  class="col-sm-3" for="exampleSelectBorder">Pilih Dokter</label>
                  <div class="col-sm-9">
                  <select class="custom-select form-control-border"  name="dokter" id="dokter">
                  <option value="">Pilih Semua Dokter</option>	
										<?php
										$no=0;
										foreach($dokter as $tampil){
										$no++;
                    					echo '<option value="'. $tampil->kode_dokter.'">'. $tampil->nama.'</option>';
               							 } ?>
										</select>
          </div>
                </div>
                    <div class="form-group">
                      <label class="col-sm-3">Rencana Tanggal Periksa</label>
                      <div class="col-sm-6">
                      <input type="text" id="datepickera" name="datepickera" class="form-control">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              </div>
              </div>

              <div class="box-footer">
              
                <button type="submit" id="btnSubmit" name="btnSubmit" class="btn btn-primary"><i class="fa fa-search"></i> Cek Jadwal</button>
                <div class="card-body table-responsive p-0">
              <table id="example2" class="table table-bordered table-striped display nowrap">

                <thead>

                <tr>          
                <th>No</th>
                  <th>Nama Poli</th>
                  <th>Nama Dokter</th>
                  <th>Hari</th>
                  <th>Jam Mulai</th>
                  <th>Jam Selesai</th>
                  <th>Pilih</th>
                </tr>

                </thead>

                <tbody>

                <!-- <?php

                  $no=0;

                  foreach($jadwal as $tampil){

                  $no++;

                ?>

                <tr>
                <td><?php echo $no;?></td>
                  <td><?php echo $tampil->nama_poli;?></td>
                  <td><?php echo $tampil->nama.', '.$tampil->gelar;?></td>
                  <td><?php 
                  $hari = $tampil->hari;
                  switch ($hari) {
                    case 0:
                        $h="Minggu";
                        break;
                    case 1:
                        $h="Senin";
                        break;
                    case 2:
                        $h="Selasa";
                        break;
                    case 3:
                        $h="Rabu";
                        break;
                    case 4:
                        $h="Kamis";
                        break;
                    case 5:
                        $h="Jumat";
                        break;
                    case 6:
                        $h="Sabtu";
                        break;
                }
                  echo $h;
                  ?></td>
                  <td><?php echo $tampil->jam_mulai;?></td>
                  <td><?php echo $tampil->jam_selesai;?></td>
                  <td>
                  
                  <button class="btn btn-sm btn-info ml-2" data-toggle="modal" data-target="#konfirmasi_modal" data-idfinal="<?php echo $tampil->idjadwal;?>"  data-polifinal="<?php echo $tampil->nama_poli;?>" data-dokterfinal="<?php echo $tampil->nama.', '.$tampil->gelar;?>" data-harifinal="<?php echo $h; ?>" data-waktufinal="<?php echo $tampil->jam_mulai.' s/d '.$tampil->jam_selesai;?>"><i class="fa fa-check-circle"></i> Pilih</button>
                   
                  </td>
                </tr>

                <?php } ?> -->

                </tbody>

              </table>
                  </div>  
              </div>
            </form>
          </div>
          <!-- /.box -->

          <!-- Form Element sizes -->
         
          <!-- /.box -->

          
          <!-- /.box -->

          <!-- Input addon -->
          
          <!-- /.box -->
        </div>
      </div>
    </section>


 <!-- Editing form modal -->
<div id="konfirmasi_modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Konfirmasi Reservasi Online</h4>
                <button type="button" class="close" data-dismiss="modal">×</button>
            </div>
            <div class="modal-body">

                <form method="post" id="konfirmasi_form">
                    <input type="hidden" name="idfinal" id="idfinal" />
                    <input type="hidden" name="tipe_pasien" id="tipe_pasien" />
                    <input type="hidden" name="tempat_lahir" id="tempat_lahir" />
                    <input type="hidden" name="tanggal_lahir" id="tanggal_lahir" />
                    <input type="hidden" name="alamat" id="alamat" />
                    <input type="hidden" name="tgl_periksa" id="tgl_periksa" />
                    <input type="hidden" name="hp" id="hp" />
                    <input type="hidden" name="nama" id="nama" />
                    <input type="hidden" name="asuransi1" id="asuransi1" />
                    <input type="hidden" name="nopeserta1" id="nopeserta1" />
                    <input type="hidden" name="rekam_medis" id="rekam_medis" />
                    
                    <h3>Apakah Anda yakin memilih jadwal berikut ini?</h3>
                    <div class="form-group">
                        <label for="polifinal">Poli</label>
                        <input type="text" name="polifinal" id="polifinal" class="form-control"/>
                    </div>
                    <div class="form-group">
                        <label for="dokterfinal">Dokter</label>
                        <input type="text" name="dokterfinal" id="dokterfinal" class="form-control"/>
                    </div>
                    <div class="form-group">
                        <label for="harifinal">Hari</label>
                        <input type="text" name="harifinal" id="harifinal" class="form-control"/>
                    </div>
                    <div class="form-group">
                        <label for="waktufinal">Waktu</label>
                        <input type="text" name="waktufinal" id="waktufinal" class="form-control"/>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
                        <input type="submit" name="update" id="update" value="Ya" class="btn btn-info" />
                    </div>
                </form>

            </div> <!-- .modal-body -->
        </div> <!-- .modal-content -->
    </div> <!-- .modal-dialog -->
</div> <!-- .modal -->

<!-- Kartu antrian form modal -->
<div id="antrian_modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Nomor Pendaftaran Online</h4>
                <button type="button" class="close" data-dismiss="modal">×</button>
            </div>
            <div class="modal-body" id="modal-body">
              
                <form method="post" id="cetakantrian_form">
                    <input type="hidden" name="no_antrian" id="no_antrian" />
                    <input type="hidden" name="nama_poli" id="nama_poli" />
                    <input type="hidden" name="nama_dokter" id="nama_dokter" />
                    <input type="hidden" name="hari_dantgl" id="hari_dantgl" />
                    <input type="hidden" name="jam_periksa" id="jam_periksa" />
                    <input type="hidden" name="tanggal_lahirpdf" id="tanggal_lahirpdf" />
                    <input type="hidden" name="nama_pasien" id="nama_pasien" />
                    <input type="hidden" name="rekam_medispdf" id="rekam_medispdf" />
                    <div id="ujicoba">
                    <table><tr><td><img src="<?php echo base_url();?>assets/image/logoklinik.png" width="30%" height="30%"></td>
                    <td colspan="2" width="200"><center><b>3K Dental Care<br>Kartu Pendaftaran Reservasi Online</center></b></td></tr></table>
                    <div class="card-body table-responsive p-0">

              <table id="example2" class="table table-bordered ">

                <tr>          
                <th>No Pendaftaran</th><td>:</td><td><span id="noantrian"></span></td></tr>
                <tr><th>Nama Poli</th><td>:</td><td><span id="namapoli"></span></td></tr>
                <tr><th>Nama Dokter</th><td>:</td><td><span id="dokterprint"></span></td></tr>
                <tr><th>Hari / Tgl. Periksa</th><td>:</td><td><span id="haridantgl"></span></td></tr>
                <tr><th>Jam Periksa</th><td>:</td><td><span id="jamperiksa"></span></td></tr>
                <tr><th>Rekam Medis</th><td>:</td><td><span id="rekammedis"></span></td></tr>
                <tr><th>Nama Pasien</th><td>:</td><td><span id="namaprint"></span></td></tr>
                <tr><th>Tgl. Lahir Pasien</th><td>:</td><td><span id="tanggallahir"></span></td></tr>
                <tr><td colspan="3" style="color:red;">NB : Nomor Pendaftaran diatas belum terkonfirmasi sebelum dilakukan konfirmasi ditempat </td></tr>
              </table>
              </div>
              </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" id="clearmodal" data-dismiss="modal">Tutup</button>
                        <button type="button" class="btn btn-info" id="btn-Preview-Image" data-dismiss="modal">Simpan ke Gambar/Image</button>
                        <input type="submit" name="simpanpdf" id="simpanpdf" value="Simpan ke PDF" class="btn btn-info" />
                    </div>
                </form>

            </div> <!-- .modal-body -->
        </div> <!-- .modal-content -->
    </div> <!-- .modal-dialog -->
</div> <!-- .modal -->

    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script type="text/javascript">
        $('#showNomerpeserta').hide();
        $("#showNomerpeserta").css('visibility', 'visible');
        $('#showNama').hide();
        $("#showNama").css('visibility', 'visible');
       
        $('#kategasuransi').hide();
        $("#kategasuransi").css('visibility', 'visible');
        var x = document.getElementById("pendaftar").value;
            if (x=='lama') {
                $('#kategdaftar').show();
                $('#showNama').hide();
            } else {
                $('#kategdaftar').hide();
                $('#showNama').show();
                $('#kategasuransi').hide();
            }
        /*$(document).ready(function() {
            $("input#nim").on("change", function() {
                var id = this.value;
                $.ajax({
                    url: "<?php //echo base_url()."Pendaftaran/getmhsw/";?>"+id,
                    type: "GET", 
                    dataType: 'json',
                    success: function (response) {
                      $("#namamhsw").val(response.result.nama_mahasiswa);
                      $("#kodekhusus").val(response.result.kode_khusus);
                      $("#prodi").val(response.result.programstudi);
                      $("#fak").val(response.result.fakultas);
                      console.log(response);
                    },
                    error: function () {
                      console.log("error");
                    }
                });
                alert('tes');
            });
        });*/
        function cekasuransi_change() {
      var no_peserta = document.getElementById("no_asuransi").value;
            $.ajax({
                url: "<?php echo base_url();?>Pendaftaran/getasuransi/"+no_peserta,
                type: "GET",
                dataType: 'json',
                cache: false,
                crossDomain: false,
                success: function (response) {
                  //  confirm("Berhasil");
                  // $("#nama_pasien").text(response.respon.NMPST);
                  // respon = JSON.parse(response.respon);
                  // $("#nama_pasien").html(respon.NMPST);
                  // $("#h_nama").val(respon.NMPST);
                  // $("#tanggal_lahir").html(respon.TGLLAHIR);
                  // $("#h_tanggal_lahir").val(respon.TGLLAHIR);
                  // $("#jenis_kelamin").html(respon.JENISKELAMIN);
                  // $("#h_jenis_kelamin").val(respon.JENISKELAMIN);
                  // $("#nama_produk").html(respon.NAMAPRODUK);
                  // $("#h_kode_produk").val(respon.KODEPRODUK);
                  // $("#nama_kelas").html(respon.NAMAKELASRAWAT);
                  // $("#h_kode_kelas").val(respon.KODEKELASRAWAT);
                  // $("#nama_mhsw").text(response.result.nama_mahasiswa);
                  // $("#kode_khusus").text(response.result.kode_khusus);
                  // $("#prodi2").text(response.result.programstudi);
                  // $("#fak2").text(response.result.fakultas);
                  // $("#kodekhusus").val(response.result.kode_khusus);
                  // $("#prodi").val(response.result.programstudi);
                  // $("#fak").val(response.result.fakultas);
                  // $("#namamhsw").attr("type","hidden");
                  //   $("#kodekhusus").attr("type","hidden");
                  //   $("#prodi").attr("type","hidden");
                  //   $("#fak").attr("type","hidden");
                  if (response.ERRORCODE == '00') {
                      $("#statusasuransi").val('Aktif');
                      $("#status_asuransi").text('Aktif');  
                      $("#atasnama").val(response.NMPST);
                      $("#atas_nama").text(response.NMPST);  
                      $("#tgllahirasuransi").val(response.TGLLAHIR);
                      $("#tgllahir_asuransi").text(response.TGLLAHIR);  
                  } else {
                      $("#statusasuransi").val('Tidak Aktif');
                      $("#status_asuransi").text('Tidak Aktif');
                  }
                  // console.log(response);
                },
                error: function(xhr, textStatus, error){
                  console.log(xhr.statusText);
                        console.log(textStatus);
                        console.log(no_peserta);
                  alert("Maaf, anda salah memasukkan No. Peserta Asuransi?");
                  $("#no_asuransi").val('');
                  $("#no_asuransi").focus();
                }
            });  
    }

    function cekasuransi3_change() {
          
         // string.rekam_medis = $("#nim").val();
         var no_peserta = document.getElementById("no_asuransi").value;
          //var id = document.getElementById("nim").value;
                $.ajax({
                    url   : "<?php echo base_url()."Pendaftaran/getasuransi/";?>"+no_peserta,
                    type  : "GET",
                    // data  : string,
                    cache : false, 
                    dataType: 'json',
                    success: function (response) {  
                      alert("Berhasil");
                      console.log(response);
                    },
                    error: function (error) {
                      console.log(error);
                      alert("Maaf, anda salah memasukkan No. Peserta Asuransi");  
                    }
                });  
        }

        function ceknim_change() {
          var string = {};
         // string.rekam_medis = $("#nim").val();
         string.namamhsw = document.getElementById("namamhsw").value;
         string.datepickerc = document.getElementById("datepickerc").value;
          //var id = document.getElementById("nim").value;
                $.ajax({
                    url   : "<?php echo base_url()."Pendaftaran/getmhsw/";?>",
                    type  : "POST",
                    data  : string,
                    cache : false, 
                    dataType: 'json',
                    success: function (response) {
                      $("#nim").val(response.rekam_medis);
                      $("#rekam_medis").text(response.rekam_medis);

                      $("#jnspasien").val(response.jenis_pasien);
                     
                      $("#nopeserta").val(response.no_peserta);
                      $("#no_peserta").text(response.no_peserta);
                      if( $("#jnspasien").val() == '1') {
                        $("#jns_pasien").text('Umum');
                        $('#showNomerpeserta').hide();
                      } else {
                        $("#jns_pasien").text('Asuransi Inhealth');
                        $('#showNomerpeserta').show();
                      }
                      $("#kode_khusus").text(response.tempat_lahir+","+response.tanggal_lahir);
                      $("#prodi2").text(response.alamat);
                      $("#fak2").text(response.hp);
                      $("#kodekhusus").val(response.tempat_lahir+","+response.tanggal_lahir);
                      $("#prodi").val(response.alamat);
                      $("#fak").val(response.hp);
                      if ($("#nim").val() == '') {
                        alert("Maaf, anda salah memasukkan Data Anda");
                        $("#namamhsw").focus();
                        $("#jns_pasien").text('');
                      $("#jnspasien").val('');
                      }
                      console.log(response);
                    },
                    error: function () {
                      console.log("error");
                      alert("Maaf, anda salah memasukkan Data Anda");
                      $("#nim").val('');
                      $("#kodekhusus").val('');
                      $("#prodi").val('');
                      $("#fak").val('');
                      $("#rekam_medis").text('');
                      $("#kode_khusus").text('');
                      $("#prodi2").text('');
                      $("#fak2").text('');
                      $("#jns_pasien").text('');
                      $("#jnspasien").val('');
                      $("#nopeserta").val('');
                      $("#no_peserta").text('');
                    }
                });
                
           
        }
        
        function status_change() {
            var x = document.getElementById("pendaftar").value;
            if (x=='lama') {
                $('#kategdaftar').show();
                $('#showNama').hide();
            } else {
                $('#kategdaftar').hide();
                $('#showNama').show();
                $('#kategasuransi').hide();
            }
          }
          function asuransi_change() {
            var x = document.getElementById("asuransi").value;
            if (x=='umum') {
                $('#kategasuransi').hide();
            } else {
                $('#kategasuransi').show();
            }
          }

$(document).ready(function() {
  $("#konfirmasi_modal").on('show.bs.modal', function(e) {
            // # relatedTarget
            // https://www.w3schools.com/jquery/event_relatedtarget.asp
            var triggerLink = $(e.relatedTarget);
            // var datepickera = document.getElementById("datepickera").value;
            // if (datepickera =='') {
            //   alert('Maaf, Silakan isi tanggal periksa');
            //   $('#konfirmasi_modal').modal('hide');
              
            //   $(this).modal('hide');
            // }
            // # get the value from data attributes from the selected row.
            // https://api.jquery.com/data/
            var idfinal = triggerLink.data("idfinal");
            var polifinal = triggerLink.data("polifinal");
            var dokterfinal = triggerLink.data("dokterfinal");
            var harifinal = triggerLink.data("harifinal");
            var waktufinal = triggerLink.data("waktufinal");

            //get data from pasien
            var tipe_pasien = document.getElementById("pendaftar").value;
            var rekam_medis = document.getElementById("nim").value;
            var nama = document.getElementById("nama_lengkap").value;
            var tempat_lahir = document.getElementById("tempat_lahir").value;
            var tanggal_lahir = document.getElementById("datepickerb").value;
            var tgl_periksa = document.getElementById("datepickera").value;
            var alamat = document.getElementById("alamat").value;
            var asuransi = document.getElementById("asuransi").value;
            var nopeserta = document.getElementById("nopeserta").value;
            var hp = document.getElementById("hp").value;        
                      
            // # assign the selected row value to the form
            // set the modal title
            $(this).find('.modal-title').text('Konfirmasi Reservasi Online');

            // # set the form
            // $(this).find(".modal-body").html("<h5>id: " + id + "</h5><p>title: " + title + "</p><p>slug: " + slug + "</p><p>text: " + text + "</p>");
            var modal_body = $(this).find(".modal-body");
            modal_body.find('#tipe_pasien').val(tipe_pasien);
            modal_body.find('#rekam_medis').val(rekam_medis);
            modal_body.find('#nama').val(nama);
            modal_body.find('#tempat_lahir').val(tempat_lahir);
            modal_body.find('#tanggal_lahir').val(tanggal_lahir);
            modal_body.find('#tgl_periksa').val(tgl_periksa);
            modal_body.find('#hp').val(hp);
            modal_body.find('#alamat').val(alamat);
            modal_body.find('#asuransi1').val(asuransi);
            modal_body.find('#nopeserta1').val(nopeserta);


            modal_body.find('#idfinal').val(idfinal); // this Id field doesn't need to set as disabled because we want to pass the value to $_POST.
            modal_body.find('#polifinal').val(polifinal).prop("disabled", true);
            modal_body.find('#harifinal').val(harifinal).prop("disabled", true);
            modal_body.find('#dokterfinal').val(dokterfinal).prop("disabled", true);
            modal_body.find('#waktufinal').val(waktufinal).prop("disabled", true);

        });
    $('#clearmodal').on("click", function() {
      $('#antrian_modal').modal('hide');
      document.location.href='<?php echo base_url();?>';
    });

    $('#btnSubmit').on("click", function() {
      cekjadwal_change();
    });
    $('#btnCari').on("click", function() {
      ceknim_change();
    });
    $('#dokter').on("change", function() {
      cekjadwal_change();
    });
    $('#poli').on("change", function() {
      cekjadwal_change();
    });
    $('#datepickera').on("change", function() {
      cekjadwal_change();
    });

    function cekjadwal_change() {
            event.preventDefault();
            var table = $('#example2');
            $.ajax({
                url: "<?php echo base_url()."Pendaftaran/getjadwal";?>",
                dataType: 'JSON',
                method: 'POST',
                data: {
                    'poli': $('#poli').val(),
                    'dokter': $('#dokter').val(),
                    'tgl_periksa': $('#datepickera').val()
                },
                success: function(data_return) {
                    console.log(data_return);

                    // destroy the DataTable
                    table.dataTable().fnDestroy();
                    // clear the table body
                    table.find('tbody').empty();
                    // reinitiate
                    table.DataTable({
                        // # data source as object (JSON object array)
                        // You must use the exactly format as shown on the link below
                        // https://datatables.net/manual/data/#Objects
                        data: data_return,
                        columns: [
                            {
                                "data": "idjadwal"
                            },
                            {
                                "data": "nama_poli"
                            },
                            {
                                "data": "nama"
                            },
                            {
                                "data": "hari"
                            },
                            {
                                "data": "jam_mulai"
                            },
                            {
                                "data": "jam_selesai"
                            },
                            {
                                "data": null
                            },
                        ],
                        columnDefs: [{
                                // # hide the first column
                                // https://datatables.net/examples/advanced_init/column_render.html                    
                                "targets": [0],
                                // "visible": false
                            },
                            {
                                // # disable search for column number 2
                                // https://datatables.net/reference/option/columns.searchable                    
                                "targets": [3],
                                "searchable": false,
                                // # disable orderable column
                                // https://datatables.net/reference/option/columns.orderable
                                "orderable": false
                            },
                            {
                                // # action controller (edit,delete)
                                "targets": [6],
                                // # column rendering
                                // https://datatables.net/reference/option/columns.render
                                "render": function(data, type, row, meta) {
                                   // $controlls = '<button class="btn btn-sm btn-info" data-toggle="modal" data-target="#edit_news_modal" data-id="' + row.id + '" data-title="' + row.title + '" data-slug="' + row.slug + '" data-text="' + row.text + '">Edit</button>';
                                    $controlls = '<button class="btn btn-sm btn-info ml-2" data-toggle="modal" data-target="#konfirmasi_modal" data-idfinal="' + row.idjadwal + '" data-polifinal="' + row.nama_poli + '" data-dokterfinal="' + row.nama + '" data-harifinal="' + row.hari + '" data-waktufinal="' + row.jam_mulai + ' s/d '+ row.jam_selesai +'"><i class="fa fa-check-circle"></i> Pilih</button>';
                                    return $controlls;
                                },
                                "width": 100
                            }
                        ],
                        // #set order descending and ascending
                        // https: //datatables.net/reference/option/order
                        "order": [
                            [1, 'desc'],
                            [2, 'asc']
                        ]
                    });
                }
            });

        }
        $('#konfirmasi_form').on("submit", function(event) {
            event.preventDefault();

            // # form validation
            // Optional, you can use the jQuery validation plugin (https://applerinquest.com/how-to-validate-a-form-with-jquery-validation-plugin/)
            // if you don't want to do the form validation manually.

            // For the warning message, you can use the Alert message from Bootstrap if you like. I use alert() because I want to keep the post short.
            // https://getbootstrap.com/docs/4.0/components/alerts/
            var edit_modal = $('#konfirmasi_modal');
            // var $title = edit_modal.find('#title');
            // var $slug = edit_modal.find('#slug');
            // var $text = edit_modal.find('#text');

            // if ($title.val() == "") {
            //     alert("Title is required");
            //     $title.focus();

            // } else if ($slug.val() == '') {
            //     alert("Slug is required");
            //     $slug.focus();

            // } else if ($text.val() == '') {
            //     alert("Text is required");
            //     $text.focus();

            // } else {
                $.ajax({
                    url: "<?php echo base_url()."Pendaftaran/simpanjadwal";?>",
                    method: "POST",
                    data: $('#konfirmasi_form').serialize(),
                    dataType: "JSON", // you must set dataType as JSON while sending the Ajax request
                    beforeSend: function() {
                        $('#update').val("Processing...");
                    },
                    success: function(response) {
                        // # get the response from our controller
                        console.log(response);
                         console.log(response.status);
                         console.log(response.data);

                        if (response.status) {
                            // reset the form
                            // $('#update_form')[0].reset();
                            $('#update_form').each(function() { this.reset() });
                            $('#update').val("Save");

                            // hide the Bootstrap modal
                            $('#konfirmasi_modal').modal('hide');
                            alert('Berhasil melakukan reservasi Online. Silakan anda menunggu konfirmasi dari petugas kami, terimakasih');
                            
                            $('#nama_poli').val(response.data.nama_poli);
                            $('#no_antrian').val(response.data.no_antrian);
                            $('#nama_dokter').val(response.data.nama_dokter);
                            $('#hari_dantgl').val(response.data.hari+' / '+response.data.tgl_periksa);
                            $('#jam_periksa').val(response.data.jam_periksa);
                            $('#tanggal_lahirpdf').val(response.data.tanggal_lahir);
                            $('#nama_pasien').val(response.data.nama_pasien);
                            $('#rekam_medispdf').val(response.data.rekam_medis);
                           
                            
                            $("#namapoli").text(response.data.nama_poli);
                            $('#noantrian').text(response.data.no_antrian);
                            $('#dokterprint').text(response.data.nama_dokter);
                            $('#haridantgl').text(response.data.hari+' / '+response.data.tgl_periksa);
                            $('#jamperiksa').text(response.data.jam_periksa);
                            $('#rekammedis').text(response.data.rekam_medis);
                            $('#namaprint').text(response.data.nama_pasien);
                            $('#tanggallahir').text(response.data.tanggal_lahir);
                            
                            $('#antrian_modal').modal('show');
                            // success message
                            
                            
                            //document.location.href='<?php echo base_url();?>';
                        } else {
                            alert('Gagal melakukan reservasi Online, silakan di cek kembali data anda!');
                           
                        }
                    },
                    error: function(xhr, status, error) {
                        var err = eval("(" + xhr.responseText + ")");
                        alert(err.Message);
                    }
                });
             // }
        });
        $('#cetakantrian_form').on("submit", function(event) {
            event.preventDefault();
            var edit_modal = $('#antrian_modal');
                $.ajax({
                    url: "<?php echo base_url()."Pendaftaran/simpanpdf";?>",
                    method: "POST",
                    data: $('#cetakantrian_form').serialize(),
                    dataType: "JSON", // you must set dataType as JSON while sending the Ajax request
                    beforeSend: function() {
                        $('#update').val("Processing...");
                    },
                    success: function(response) {
                        // # get the response from our controller
                        console.log(response);
                         console.log(response.status);
                         console.log(response.data);

                        if (response.status) {
                            // reset the form
                            // $('#update_form')[0].reset();
                            $('#update_form').each(function() { this.reset() });
                            $('#update').val("Save");
                            // hide the Bootstrap modal
                            $('#antrian_modal').modal('hide');

                        } else {
                            alert('Gagal Simpan ke PDF');
                           
                        }
                    },
                    error: function(xhr, status, error) {
                        var err = eval("(" + xhr.responseText + ")");
                        alert(err.Message);
                    }
                });
             // }
        });
}); // document ready ends
$(document).ready(function() {


            var element = $("#ujicoba"); // global variable
            var getCanvas; // global variable
            var newData;

            $("#btn-Preview-Image").on('click', function() {
                html2canvas(element, {
                    onrendered: function(canvas) {
                        getCanvas = canvas;
                        var imgageData = getCanvas.toDataURL("image/jpg");
                        var a = document.createElement("a");
                        a.href = imgageData; //Image Base64 Goes here
                        a.download = "Kartu Pendaftaran "+document.getElementById("nama_lengkap").value+"-"+document.getElementById("datepickera").value+".jpg"; //File name Here
                        a.click(); //Downloaded file
                    }
                });
            });


        });

        $(document).ready(function() {


var element = $("#html-content-holder"); // global variable
var getCanvas; // global variable
var newData;

$("#btn-Preview-Image").on('click', function() {
    html2canvas(element, {
        onrendered: function(canvas) {
            getCanvas = canvas;
            var imgageData = getCanvas.toDataURL("image/jpg");
            var a = document.createElement("a");
            a.href = imgageData; //Image Base64 Goes here
            a.download = "Kartu Pendaftaran "+document.getElementById("nama_lengkap").value+"-"+document.getElementById("datepickera").value+".jpg"; //File name Here
            a.click(); //Downloaded file
        }
    });
});

});
    </script>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <strong>Copyright &copy; 2023</strong> All rights
    reserved.
  </footer>
  <!-- Control Sidebar -->
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="<?php echo base_url();?>assets/plugins/jquery/dist/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="<?php echo base_url();?>assets/plugins/jQueryUI/jquery-ui.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
 <script type="text/javascript" src="<?php echo base_url();?>assets/ckeditor/ckeditor.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url();?>assets/plugins/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="<?php echo base_url();?>assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<!-- datepicker -->
<script src="<?php echo base_url();?>assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<!-- Slimscroll -->
<!-- FastClick -->
<script src="<?php echo base_url();?>assets/plugins/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url();?>assets/dist/js/adminlte.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?php echo base_url();?>assets/dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url();?>assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<script>
  $(function () {
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace('editor1')
    //bootstrap WYSIHTML5 - text editor
    $('.textarea').wysihtml5()
  })
</script>
<script src="<?php echo base_url();?>assets/dist/js/demo.js"></script>
<script>

  $('#datepicker').datepicker({
      autoclose: true,
      format:'yyyy-mm-dd'
    });
  $('#datepickera').datepicker({
      autoclose: true,
      format:'yyyy-mm-dd'
    });
  $('#datepickerb').datepicker({
      autoclose: true,
      format:'yyyy-mm-dd',
      changeMonth: true,
      changeYear: true,
      showButtonPanel: true
    });
    $('#datepickerc').datepicker({
      autoclose: true,
      format:'yyyy-mm-dd',
      changeMonth: true,
      changeYear: true,
      showButtonPanel: true
    });
  $('.timepicker').timepicker({
      showInputs: false
    });
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': true,
      'searching'   : true,
      'ordering'    : false,
      'info'        : true,
      'responsive'  : true,
      'autoWidth'   : false
    });
        $('#example3').DataTable({
      'paging'      : false,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : false,
      'info'        : false,
      'autoWidth'   : false
    });
    $('#example4').DataTable({
      'scrollY'     : true,
      'autoWidth'   : false
    });

  });
  
     
</script>
</body>
</html>
