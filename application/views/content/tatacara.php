<style>
  #showNama {
    display: none; // you're using diaplay!
  }
</style>
<?php
$error = $this->uri->segment(4);
//header('Content-Type: application/json');
header('Access-Control-Allow-Origin: *');
?>
<section class="content-header">
  <h1>
    Petunjuk Instalasi Aplikasi Ujian BBJ
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Form</a></li>
    <li class="active">Petunjuk Instalasi Aplikasi</li>
  </ol>
</section>

<section class="content">
  <div class="row">
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="box box-primary">
        <form role="form" class="form-horizontal" action="" method="POST" enctype="multipart/form-data">
          <div class="box-body">
            <div class="row">
              <div class="col-md-12">

                <div id="kategdaftar" style="display: inline;">
                  <div class="form-group">
                    <div class="col-sm-12" style="font-size: 16px">
                      <ol>
                        <li>Unduh aplikasi pada tombol "Unduh untuk Windows" dibawah</li>
                        <li>Extract file EaxamBBJ.zip yang sudah diunduh</li>
                        <li>Double klik file InstallBBJUSMExam.bat</li>
                        <li>Ikuti Langkah selanjutnya dan tunggu hingga proses selesai</li>
                        <li>Setelah installasi BBJUSMExamBrowser selesai, jalankan file BBJUSM.seb</li>
                        <li>Masukkan password "bbjusm" (tanpa tanda petik)</li>
                        <li>Jika Berhasil maka halaman login akan terbuka.</li>
                        <li><font color='red'>Aplikasi ini hanya bisa dijalankan di komputer / laptop dengan OS Windows</font></li>
                      </ol> 
                    </div>
                    <div class="col-sm-12" align="center" style="margin-top: 20px">
                      <a target="_blank" href="https://bit.ly/ExamBBJUSM" class="btn btn-success">Unduh untuk Windows</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
      </div>
      </form>
    </div>
    <!-- /.box -->

    <!-- Form Element sizes -->

    <!-- /.box -->


    <!-- /.box -->

    <!-- Input addon -->

    <!-- /.box -->
  </div>
  </div>
</section>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>