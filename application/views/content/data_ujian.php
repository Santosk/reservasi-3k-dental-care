<?php
$berhasil=$this->uri->segment(5);
$email = $this->session->userdata('email');
$date = date("Y-m-d");

//data ujian
$ujian=$this->Sop_Model->qw("t_penjadwalan.*, t_kat_soal.kategori","t_kat_soal, t_penjadwalan","WHERE t_kat_soal.id=t_penjadwalan.tipe_ujian AND t_penjadwalan.tanggal>='$date' AND t_penjadwalan.status!='3' ORDER BY t_penjadwalan.tanggal DESC, t_penjadwalan.waktu_mulai DESC")->result();

$info_user = $this->Sop_Model->qw("tblkelas.ID, tblsiswa.id_siswa","tblkelas, tblsiswa","WHERE tblkelas.Kelas=tblsiswa.nis AND tblsiswa.nis='$email'")->row_array();

if($berhasil!='')
{
    $kata = "Tambah";
    $call = "success";
}


?>
<section class="content-header">

      <h1>
        Pendaftaran Ujian
      </h1>

      <ol class="breadcrumb">

        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>

        <li><a href="#">Data</a></li>

        <li class="active">Pendaftaran Ujian</li>

      </ol>

</section>



   <section class="content">

      <div class="row">

        <div class="col-xs-12">

          <div class="box box-primary">
            <?php
            if(isset($berhasil))
            {
              ?>
              <br>
              <div class="callout callout-<?php echo $call?>" style="margin-left: 10px; margin-right: 10px">
              <h4>Pendaftaran Ujian Berhasil</h4>
              <p>
                Pendaftaran Ujian berhasil dilakukan, tunggu konfirmasi pendaftaran oleh Admin untuk dapat mengikuti ujian.
              </p>
              </div>
              <?php
            }
            ?>
            <div class="box-body">

              <table id="example4" class="table table-bordered table-striped display">

                <thead>

                <tr>

                  <th>No</th>
                  <th>Kode Jadwal</th>
                  <th>Jenis Ujian</th>
                  <th>Biaya (Rp.)</th>
                  <th>Waktu Pelatihan</th>
                  <th>Waktu Ujian</th>
                  <th>Kuota / Peserta</th>
                  <th></th>

                </tr>

                </thead>

                <tbody>

                <?php

                  $no=0;

                  foreach($ujian as $tampil){

                  $no++;

                ?>

                <tr>

                  <td><?php echo $no;?></td>
                  <td><?php echo $tampil->kode_jadwal?></td>
                  <td><?php
                  echo $tampil->kategori;
                  echo "<br>(";
                  $biaya = $this->Sop_Model->qw("*","t_biaya_ujian","WHERE jenis_ujian='$tampil->jenis_ujian' AND status='1'")->row_array();
                  echo $biaya['nama_jenis_ujian'];
                  echo ")";
                  ?></td>
                  <td><?php echo number_format($biaya['biaya'])?></td>
                  <td><?php
                  if($tampil->pelatihan_mulai=='')
                    echo "-";
                  else{
                    echo date("d-m-Y", strtotime($tampil->tanggal_pelatihan));
                    echo "<br>";
                    echo $tampil->pelatihan_mulai?> - <?php echo $tampil->pelatihan_selesai;
                  }
                  ?></td>
                  <td><?php echo date("d-m-Y", strtotime($tampil->tanggal));
                  echo "<br>";
                  echo $tampil->waktu_mulai?> - <?php echo $tampil->waktu_selesai;
                  ?></td>
                  <td><?php echo $tampil->kuota;
                  echo " / ";
                  //hitung peserta
                  $peserta = $this->Sop_Model->qw("*","t_peserta_ujian","WHERE id_penjadwalan='$tampil->id_penjadwalan' AND status='1'")->num_rows();
                  echo $peserta;
                  ?></td>
                  <td>
                    <?php
                    //cek keikutsertaan
                    $ikut = $this->Sop_Model->qw("*","t_peserta_ujian","WHERE id_penjadwalan='$tampil->id_penjadwalan' AND id_peserta='$info_user[id_siswa]'");
                    $sudah_ikut = $ikut->num_rows();
                    if($sudah_ikut=='0')
                    {
                      if($peserta>=$tampil->kuota)
                      {
                        echo "<font color=red>Kuota Penuh</font>";
                      }else{
                        ?>
                        <a href="<?php echo site_url("Sop_Controller/simpan_daftar_ujian/".$tampil->id_penjadwalan."/".$info_user['id_siswa']."/".$info_user['ID']); ?>" class="btn btn-sm btn-primary" onClick="return confirm('Anda yakin akan mengikuti ujian ini?'); if (ok) return true; else return false"><i class="fa fa-plus"></i> Daftar</a>
                        <?php
                      }
                    }else{
                      $data_ikut = $ikut->row_array();
                      if($data_ikut['status']=='0')
                      {
                        $info_ujian = $this->Sop_Model->qw("*","t_peserta_ujian","WHERE id_penjadwalan='$tampil->id_penjadwalan' AND id_peserta='$info_user[id_siswa]' AND id_kelas='$info_user[ID]'")->row_array();
                        $id_peserta_ujian = $info_ujian['id_peserta_ujian'];
                        ?>
                        <a target="_blank" href="<?php echo site_url("Sop_Controller/unduh_invoice/".$tampil->id_penjadwalan."/".$id_peserta_ujian); ?>" class="btn btn-sm btn-info">Unduh Invoice</a>
                        <a href="<?php echo site_url("Sop_Controller/page/daftar_ujian/".$tampil->id_penjadwalan."/".$id_peserta_ujian); ?>" class="btn btn-sm btn-warning"><i class="fa fa-cloud-upload"></i> Upload Bukti</a>
                        <?php
                      }
                      elseif($data_ikut['status']=='2')
                        echo "<font color=orange>Belum Dikonfirmasi</font>";
                      else{
                        $info_ujian = $this->Sop_Model->qw("*","t_peserta_ujian","WHERE id_penjadwalan='$tampil->id_penjadwalan' AND id_peserta='$info_user[id_siswa]' AND id_kelas='$info_user[ID]'")->row_array();
                        $id_peserta_ujian = $info_ujian['id_peserta_ujian'];
                        echo "<font color=green>Sudah Dikonfirmasi</font><br>";
                        ?>
                        <a target="_blank" href="<?php echo site_url("Sop_Controller/unduh_kartu/".$tampil->id_penjadwalan."/".$id_peserta_ujian); ?>" class="btn btn-sm btn-info">Unduh Kartu Ujian</a>
                        <?php
                      }
                    }
                    ?>
                  </td>

                </tr>

                <?php } ?>

                </tbody>

              </table>

            </div>
            

            

            <!-- /.box-body -->

          </div>

          <!-- /.box -->

        </div>

        <!-- /.col -->

      </div>

      <!-- /.row -->

    </section>