<?php
$berhasil=$this->uri->segment(5);
$email = $this->session->userdata('email');
$date = date("Y-m-d");

//data ujian
$ujian=$this->Sop_Model->qw("pu.*, j.*","t_peserta_ujian pu 
left outer join t_penjadwalan j on pu.id_penjadwalan=j.id_penjadwalan
left outer join t_kat_soal ks on j.tipe_ujian=ks.id
left outer join t_peserta p on pu.id_peserta=p.id",
"WHERE p.username='$email' ORDER BY j.tanggal DESC, j.waktu_mulai DESC")->result();

$info_user = $this->Sop_Model->qw("tblkelas.ID, tblsiswa.id_siswa","tblkelas, tblsiswa","WHERE tblkelas.Kelas=tblsiswa.nis AND tblsiswa.nis='$email'")->row_array();

if($berhasil!='')
{
    $kata = "Tambah";
    $call = "success";
}


?>
<section class="content-header">

      <h1>
        History Ujian
      </h1>

      <ol class="breadcrumb">

        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>

        <li><a href="#">Data</a></li>

        <li class="active">History Ujian</li>

      </ol>

</section>



   <section class="content">

      <div class="row">

        <div class="col-xs-12">

          <div class="box box-primary">
            <?php
            if(isset($berhasil))
            {
              ?>
              <br>
              <div class="callout callout-<?php echo $call?>" style="margin-left: 10px; margin-right: 10px">
              <h4>Pendaftaran Ujian Berhasil</h4>
              <p>
                Pendaftaran Ujian berhasil dilakukan, tunggu konfirmasi pendaftaran oleh Admin untuk dapat mengikuti ujian.
              </p>
              </div>
              <?php
            }
            ?>
            <div class="box-body">

              <table id="example4" class="table table-bordered table-striped display">

                <thead>

                <tr>

                  <th>No</th>
                  <th>NO. TFL</th>
                  <th>LISTENING</th>
                  <th>STRUCTURE</th>
                  <th>READING</th>
                  <th>TOTAL</th>
                  <th>STATUS</th>
                  <th></th>

                </tr>

                </thead>

                <tbody>

                <?php

                  $no=0;

                  foreach($ujian as $tampil){

                  $no++;

                ?>

                <tr>

                  <td><?php echo $no;?></td>
                  <td><?php echo $tampil->no_reg?></td>
                  <td><?php
                  //list ujian
                  $detilujian=$this->db->query(
                    "SELECT * FROM t_jadwal_ujian WHERE id_penjadwalan='$tampil->id_penjadwalan' 
                    ORDER BY kode_part")->result_array();
                  $true_answer = 0;
                  foreach ($detilujian as $k_ujian => $v_ujian) {
                    $q = $this->db->query("select p.kode_mapel, p.jawaban_benar,ju.id_penjadwalan from t_pengerjaan p 
                      LEFT OUTER JOIN t_jadwal_ujian ju on p.id_ujian=ju.id_ujian 
                      where p.id_peserta=".$tampil->id_peserta." 
                      and p.kode_mapel=".$v_ujian['kode_part']." and ju.id_penjadwalan=".$v_ujian['id_penjadwalan']." LIMIT 1");
                    if($q->num_rows() > 0) {
                    foreach ($q->result() as $row)
                    {
                    if ($row->kode_mapel == 1 || $row->kode_mapel == 2 || $row->kode_mapel == 3) {
                      if (empty($row->jawaban_benar) || $row->jawaban_benar=='') $jawaban_benar=0;
                      else $jawaban_benar=$row->jawaban_benar;
                      $true_answer = $true_answer + $jawaban_benar;
                    
                    }
            }
            }
                  }
                  $skor = $this->Sop_Model->convert_nilai($true_answer,1);
                  echo $skor;
                  
                  ?></td>
                  <td>
                  <?php 
    $true_answer2 = 0;
    foreach ($detilujian as $k_ujian => $v_ujian): ?>
        
            <?php 
            //echo $v_nilai['nilai_'.$v_bidang['kode']]; 
            $q2 = $this->db->query("select p.kode_mapel, p.jawaban_benar,ju.id_penjadwalan from t_pengerjaan p 
            LEFT OUTER JOIN t_jadwal_ujian ju on p.id_ujian=ju.id_ujian 
            where p.id_peserta=".$tampil->id_peserta." 
            and p.kode_mapel=".$v_ujian['kode_part']." and ju.id_penjadwalan=".$v_ujian['id_penjadwalan']." LIMIT 1");
            if($q2->num_rows() > 0) {
            foreach ($q2->result() as $row)
            {
                if ($row->kode_mapel == 4 || $row->kode_mapel == 5 || $row->kode_mapel == 6) {
                    if (empty($row->jawaban_benar) || $row->jawaban_benar=='') $jawaban_benar=0;
                    else $jawaban_benar=$row->jawaban_benar;
                    $true_answer2 = $true_answer2 + $jawaban_benar;
                    
                }
            }
            }
            
            ?>
    <?php endforeach; 
    $skor2 = $this->Sop_Model->convert_nilai($true_answer2,4);
    echo $skor2;
    ?>
                  </td>
                  <td>
                  <?php 
    $true_answer3 = 0;
    foreach ($detilujian as $k_ujian => $v_ujian): ?>
        
            <?php 
            //echo $v_nilai['nilai_'.$v_bidang['kode']]; 
            $q3 = $this->db->query("select p.kode_mapel, p.jawaban_benar,ju.id_penjadwalan from t_pengerjaan p 
            LEFT OUTER JOIN t_jadwal_ujian ju on p.id_ujian=ju.id_ujian 
            where p.id_peserta=".$tampil->id_peserta." 
            and p.kode_mapel=".$v_ujian['kode_part']." and ju.id_penjadwalan=".$v_ujian['id_penjadwalan']." LIMIT 1");
            if($q3->num_rows() > 0) {
            foreach ($q3->result() as $row)
            {
                if ($row->kode_mapel == 7 || $row->kode_mapel == 8 || $row->kode_mapel == 9) {
                    if (empty($row->jawaban_benar) || $row->jawaban_benar=='') $jawaban_benar=0;
                    else $jawaban_benar=$row->jawaban_benar;
                    $true_answer3 = $true_answer3 + $jawaban_benar;
                    
                }
            }
            }
            
            ?>
    <?php endforeach; 
    $skor3 = $this->Sop_Model->convert_nilai($true_answer3,7);
    echo $skor3;
    ?>

                  </td>
                  <td><?php 
    $akhirSkor = (($skor+ $skor2 +$skor3)/3)*10;
    echo round($akhirSkor); ?></td>
                  <td>
                  <?php 
    $status='';
    if ($akhirSkor>=400) {
        $status="Lulus";
    } else $status="Gagal";
    echo $status;
    ?>
                  </td>
                  

                </tr>

                <?php } ?>

                </tbody>

              </table>

            </div>
            

            

            <!-- /.box-body -->

          </div>

          <!-- /.box -->

        </div>

        <!-- /.col -->

      </div>

      <!-- /.row -->

    </section>