<?php
  $id_penjadwalan = $this->uri->segment(4);
  $id_peserta_ujian=$this->uri->segment(5);
  $notif=$this->uri->segment(6);
  $email = $this->session->userdata('email');
  
  $info=$this->Sop_Model->qw("t_penjadwalan.*, t_kat_soal.kategori, t_biaya_ujian.nama_jenis_ujian","t_kat_soal, t_penjadwalan, t_biaya_ujian","WHERE t_kat_soal.id=t_penjadwalan.tipe_ujian AND t_penjadwalan.id_penjadwalan='$id_penjadwalan' AND t_penjadwalan.jenis_ujian=t_biaya_ujian.jenis_ujian AND t_biaya_ujian.status='1'")->row_array();

  $info_user = $this->Sop_Model->qw("tblkelas.ID, tblsiswa.id_siswa, tblsiswa.nim","tblkelas, tblsiswa","WHERE tblkelas.Kelas=tblsiswa.nis AND tblsiswa.nis='$email'")->row_array();

  $info_bayar = $this->Sop_Model->qw("*","t_peserta_ujian","WHERE id_peserta_ujian='$id_peserta_ujian'")->row_array();

  $open='Sop_Controller/simpan_upload_bukti';
  $kembali='Sop_Controller/page/data_ujian/';
?>
<section class="content-header">
      <h1>
        Upload Bukti Pembayaran
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Form</a></li>
        <li class="active">Daftar Ujian</li>
      </ol>
    </section>

    <section class="content">
      <div class="row">
        <div class="col-md-12">

          <?php
          if($notif=='err_ekstensi')
          {
          ?>
            <div class="callout callout-danger">
            <h4>Ekstensi / Jenis File Salah</h4>
            <p>
              File yang diperbolehkan hanya file berekstensi JPEG/JPG/PNG/PDF
            </p>
            </div>
          <?php
          }elseif($notif=='err_size')
          {
          ?>
            <div class="callout callout-danger">
            <h4>File Terlalu Besar</h4>
            <p>
              File yang diupload melebihi 2MB, kecilkan pixel kamera saat foto bukti
            </p>
            </div>
          <?php
          }elseif($notif=='err_sudah')
          {
          ?>
            <div class="callout callout-danger">
            <h4>Anda Sudah Melakukan Presensi</h4>
            <p>
              Anda sudah melakukan presensi untuk kegiatan ini. Lihat histori kegiatan
            </p>
            </div>
          <?php
          }
          ?>



          <!-- general form elements -->
          <div class="box box-primary">
            <form role="form" class="form-horizontal" action="<?php echo site_url($open);?>" method="POST" enctype="multipart/form-data">
              <input name="id_penjadwalan" value="<?php echo $id_penjadwalan?>" type="hidden">
              <input name="id_kelas" value="<?php echo $info_user['ID']?>" type="hidden">
              <input name="id_peserta" value="<?php echo $info_user['id_siswa']?>" type="hidden">
              <input name="id_peserta_ujian" value="<?php echo $id_peserta_ujian?>" type="hidden">
              <div class="box-body">
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label class="col-sm-2">No. Registrasi / NIM</label>
                      <div class="col-sm-5">
                        <?php
                          echo $info_bayar['no_reg'];
                          echo " / ";
                          echo $info_user['nim'];
                        ?>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-2">Kode Jadwal</label>
                      <div class="col-sm-5">
                        <?php
                          echo $info['kode_jadwal'];
                        ?>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-2">Nama Ujian (Kelas)</label>
                      <div class="col-sm-5">
                        <?php
                          echo $info['kategori'];
                          echo " (";
                          echo $info['nama_jenis_ujian'];
                          echo ")";
                        ?>
                      </div>
                    </div>
                    <?php
                    if($info['jenis_ujian']=='1')
                    {
                      ?>
                       <div class="form-group">
                          <label class="col-sm-2">Tanggal Pelatihan</label>
                          <div class="col-sm-3">
                            <font size="4"><b>
                           <?php
                             echo date("d-m-Y",strtotime($info['tanggal_pelatihan']));
                           ?>
                            </b></font>
                          </div>
                      </div>
                      <div class="form-group">
                          <label class="col-sm-2">Waktu Pelatihan</label>
                          <div class="col-sm-3">
                            <font size="4"><b>
                           <?php
                             echo $info['pelatihan_mulai'];
                             echo " - ";
                             echo $info['pelatihan_selesai'];
                           ?>
                            </b></font>
                          </div>
                      </div>
                      <?php
                    }
                    ?>
                    <div class="form-group">
                        <label class="col-sm-2">Tanggal Ujian</label>
                        <div class="col-sm-3">
                          <font size="4"><b>
                         <?php
                           echo date("d-m-Y",strtotime($info['tanggal']));
                         ?>
                          </b></font>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2">Waktu</label>
                        <div class="col-sm-3">
                          <font size="4"><b>
                         <?php
                           echo $info['waktu_mulai'];
                           echo " - ";
                           echo $info['waktu_selesai'];
                         ?>
                          </b></font>
                        </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-2">Rincian Biaya (Rp.)</label>
                      <div class="col-sm-1" align="right">
                        <?php echo number_format($info_bayar['biaya'])?>
                      </div>
                      <div class="col-sm-3">
                        (Biaya Ujian)
                      </div>
                    </div>
                    <div class="form-group" style="margin-top: -15px">
                      <label class="col-sm-2"></label>
                      <div class="col-sm-1" align="right">
                        <?php echo number_format($info_bayar['random'])?>
                      </div>
                      <div class="col-sm-2">
                        (Angka Random)
                      </div>
                    </div>
                    <div class="form-group" style="margin-top: -10px">
                      <label class="col-sm-2"></label>
                      <div class="col-sm-1" align="right" style="font-size: 15px">
                        <b><?php echo number_format($info_bayar['total_biaya'])?></b>
                      </div>
                      <div class="col-sm-2" style="font-size: 15px">
                        <b>(Total Biaya)</b>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-2">Bukti Bayar</label>
                      <div class="col-sm-3">
                        <input type="file" name="foto_bukti" class="form-control">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-2"></label>
                      <div class="col-sm-3">
                        <font color="red">Ekstensi File : <b>JPG/JPEG/PNG/PDF</b></font><br>
                        <font color="red">Besar File Maksimal : <b>1MB</b></font>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              </div>
              </div>

              <div class="box-footer">
                <button type="submit" name="" class="btn btn-primary"><i class="fa fa-save"></i> Upload</button>
                  <a href="<?php echo site_url($kembali);?>" class="btn btn-danger"><i class="fa fa-close"></i> Batal</a>
              </div>
            </form>
          </div>
          <!-- /.box -->

          <!-- Form Element sizes -->
         
          <!-- /.box -->

          
          <!-- /.box -->

          <!-- Input addon -->
          
          <!-- /.box -->
        </div>
      </div>
    </section>

<script src="https://adminlte.io/themes/AdminLTE/bower_components/ckeditor/ckeditor.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="https://adminlte.io/themes/AdminLTE/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<script>
  $(function () {
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace('editor1')
    //bootstrap WYSIHTML5 - text editor
    $('.textarea').wysihtml5()
  })
</script>