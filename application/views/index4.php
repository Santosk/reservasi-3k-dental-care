<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>3K Dental Care</title>
	<!-- Mobile Specific Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<!-- Font-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/theme1/css/opensans-font.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/theme1/fonts/material-design-iconic-font/css/material-design-iconic-font.min.css">
	<!-- datepicker -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/theme1/css/jquery-ui.min.css">
	<!-- Main Style Css -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/theme1/css/style.css"/>
	<link rel="stylesheet" href="<?php echo base_url();?>assets/select2/dist/css/select2.min.css"  type='text/css'/>
</head>
<body>
	<div class="page-content">
  
		<div class="wizard-heading"><img style="margin-bottom:-50px" width="90%" src="<?php echo base_url();?>assets/image/logo.png" alt="Logo Dental care"></div>
    
		<div class="wizard-v6-content">
			<div class="wizard-form">
      <h1 style="color:#ccb26e; text-align: center; font-weight:bold; margin-bottom:-10px">Reservasi Online</h1>
		        <form class="form-register" id="form-register" action="#" method="post">
		        	<div id="form-total">
		        		<!-- SECTION 1 -->
			            <h2>
			            	<p class="step-icon"><span>1</span></p>
			            	<span class="step-text">Pilih Jadwal Dokter</span>
			            </h2>
			            <section>
			                <div class="inner">
			                	<div class="form-heading">
			                		<h3>Pilih Jadwal Dokter</h3>
			                		<span>1/3</span>
			                	</div>
								<form role="form" class="form-register" action="<?php echo site_url("Pendaftaran");?>" method="POST" enctype="multipart/form-data">
								<div class="form-row">
									<div class="form-holder form-holder-2">
										<label for="room" class="special-label-1">Dokter :</label>
										<select name="dokter" id="dokter" class="form-control">
										<option value="">Pilih Semua Dokter</option>	
										<?php
										$no=0;
										foreach($dokter as $tampil){
										$no++;
                    					echo '<option value="'. $tampil->kode_dokter.'">'. $tampil->nama.'</option>';
               							 } ?>
										</select>
										<span class="select-btn">
											<i class="zmdi zmdi-chevron-down"></i>
										</span>
										
									</div>
								</div>
                
								
								<div class="form-row">
									<div class="form-holder form-holder-2">
										<label for="room" class="special-label-1">Poli :</label>
										<select name="poli" id="poli" class="form-control">
										<option value="">Pilih Semua Poli</option>
										<?php
										$no=0;
										foreach($poli as $tampil){
										$no++;
                    					echo '<option value="'. $tampil->kode_poli.'">'. $tampil->nama_poli.'</option>';
               							 } ?>
										</select>
										<span class="select-btn">
											<i class="zmdi zmdi-chevron-down"></i>
										</span>
									</div>
								</div>
								<div class="form-row">
									<div class="form-holder form-holder-2">
										<label for="room" class="special-label-1">Pilih Hari :</label>
										<select name="hari" id="hari" class="form-control">
										<option value="">Pilih Semua Hari</option>
										<option value="">Senin</option>
										<option value="">Selasa</option>
										<option value="">Rabu</option>
										<option value="">Kamis</option>
										<option value="">Jumat</option>
										<option value="">Sabtui</option>
										<option value="">Minggu</option>
										</select>
										<span class="select-btn">
											<i class="zmdi zmdi-chevron-down"></i>
										</span>
									</div>
								</div>
								<span class="select-btn">
									<button type="submit" onClick="show_alert()" id="tombolcari" name="tombolcari" class="btn btn-primary"><i class="fa fa-save"></i> Cari</button>
								</span>
							</div>
							
			            </section>
						<!-- SECTION 2 -->
			            <h2>
			            	<p class="step-icon"><span>2</span></p>
			            	<span class="step-text">Verifikasi Pasien</span>
			            </h2>
			            <section>
			                <div class="inner">
			                	<div class="form-heading">
			                		<h3>Verifikasi Pasien</h3>
			                		<span>2/3</span>
			                	</div>
		                		
                        <div class="form-row">
									<div class="form-holder">
										<label class="form-row-inner">
											<input type="text" class="form-control" id="first_name" name="first_name" required>
											<span class="label">No. RM</span>
										</label>
									</div>
									
								</div>
								
                <div class="form-row form-row-date">
									<div class="form-holder ">
										<label for="date" class="special-label-1">Tanggal Lahir:</label>
										<input type="text" id="datepicker">
									</div>
								</div>
							</div>
			            </section>
			            <!-- SECTION 3 -->
			            <h2>
			            	<p class="step-icon"><span>3</span></p>
			            	<span class="step-text">Buat Reservasi</span>
			            </h2>
			            <section>
			                <div class="inner">
			                	<div class="form-heading">
			                		<h3>Data Sosial</h3>
			                		<span>3/3</span>
			                	</div>
								<div class="table-responsive">
									<table class="table">
										<tbody>
											<tr class="space-row">
												<th>Full Name:</th>
												<td id="fullname-val"></td>
											</tr>
											<tr class="space-row">
												<th>Room:</th>
												<td id="room-val"></td>
											</tr>
											<tr class="space-row">
												<th>Day:</th>
												<td id="day-val"></td>
											</tr>
											<tr class="space-row">
												<th>Time:</th>
												<td id="time-val"></td>
											</tr>
											<tr class="space-row">
												<th>Price:</th>
												<td id="price-val">40.00$</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
			            </section>
		        	</div>
		        </form>
			</div>
		</div>
	</div>
	<script src="<?php echo base_url();?>assets/theme1/js/jquery-3.3.1.min.js"></script>
	<script src="<?php echo base_url();?>assets/theme1/js/jquery.steps.js"></script>
	<script src="<?php echo base_url();?>assets/theme1/js/jquery-ui.min.js"></script>
	<script src="<?php echo base_url();?>assets/theme1/js/main.js"></script>
	<script src="<?php echo base_url();?>assets/select2/dist/js/select2.min.js"></script>
  <script>
  $( function() {
    $( "#datepicker" ).datepicker({
      changeMonth: true,
      changeYear: true
    });
  } );

//   $("#dokter").select2();
  </script>
</body>
</html>